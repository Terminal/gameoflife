package cells;

import static org.junit.Assert.*;

import org.junit.Test;

public class DeadCellFactoryTest
{
	@Test
	public void testCreateCell()
	{
		DeadCellFactory factory = new DeadCellFactory();
		int x = 10;
		int y = 20;
		ICell cell = factory.createCell(x, y);
		
		assertEquals(x, cell.getX());
		assertEquals(y, cell.getY());
		assertFalse(cell.isAlive());
	}
}
