package configuration;

import cells.ICellFactory;

public class DefaultFieldConfiguration implements IFieldConfiguration
{

	ICellFactory inner;
	ICellFactory outer;
	ICellFactory initial;

	public DefaultFieldConfiguration(ICellFactory inner, ICellFactory outer, ICellFactory initial)
	{
		this.inner = inner;
		this.outer = outer;
		this.initial = initial;
	}

	@Override
	public void setInnerCellFactory(ICellFactory factory)
	{
		if (factory == null)
			throw new IllegalArgumentException("the factory is null");

		this.inner = factory;
	}

	@Override
	public void setOuterCellFactory(ICellFactory factory)
	{
		if (factory == null)
			throw new IllegalArgumentException("the factory is null");

		this.outer = factory;
	}

	@Override
	public ICellFactory getInnerCellFactory()
	{
		return this.inner;
	}

	@Override
	public ICellFactory getOuterCellFactory()
	{
		return this.outer;
	}

	@Override
	public ICellFactory getInitialCellFactory()
	{
		return this.initial;
	}

}
