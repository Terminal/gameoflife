package configuration;

import cells.ICellFactory;

public interface IFieldConfiguration
{
	/**
	 * Configures the cell factory which is used for inner cells
	 * 
	 * @param factory the factory which is used for inner cells
	 */
	public void setInnerCellFactory(ICellFactory factory);

	/**
	 * Configures the cell factory which is used for outer cells
	 * 
	 * @param factory the factory which is used for outer cells
	 */
	public void setOuterCellFactory(ICellFactory factory);

	/**
	 * Returns the cell factory used for inner cells
	 * 
	 * @return the cell factory used for inner cells
	 */
	public ICellFactory getInnerCellFactory();

	/**
	 * Returns the cell factory used for outer cells
	 * 
	 * @return the cell factory used for outer cells
	 */
	public ICellFactory getOuterCellFactory();

	/**
	 * Returns the cell factory used for the initial creation of cells
	 * 
	 * @returns the cell factory used for the initial creation of cells
	 */
	public ICellFactory getInitialCellFactory();
}
