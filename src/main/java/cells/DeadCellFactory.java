package cells;

/**
 * Creates a cell which is initially dead
 */
public class DeadCellFactory implements ICellFactory
{
	@Override
	public ICell createCell(int x, int y)
	{
		ICell cell = new Cell(x, y);
		return cell;
	}
}
