package cells;

public class Cell implements ICell
{
	private int x, y;

	/** the actual state of the cell */
	private boolean alive = false;

	/** the state which will be written to alive as soon as commitAlive is called */
	private boolean pendingAlive = false;

	public Cell(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	@Override
	public int getX()
	{
		return this.x;
	}

	@Override
	public int getY()
	{
		return this.y;
	}

	@Override
	public void setAlive(boolean alive)
	{
		this.pendingAlive = alive;
	}

	@Override
	public boolean isAlive()
	{
		return this.alive;
	}

	@Override
	public void commitAlive()
	{
		this.alive = this.pendingAlive;
	}
}
