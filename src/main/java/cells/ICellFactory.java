package cells;

public interface ICellFactory
{
	/**
	 * Creates a cell with the supplied coordinates
	 * 
	 * @param x the x coordinate of the cell
	 * @param y the y coordinate of the cell
	 * @return a new cell
	 */
	public ICell createCell(int x, int y);
}
