package cells;

/**
 * Creates a cell which is initially living
 */
public class LivingCellFactory implements ICellFactory
{

	@Override
	public ICell createCell(int x, int y)
	{
		ICell cell = new Cell(x, y);
		cell.setAlive(true);
		cell.commitAlive();
		return cell;
	}

}
