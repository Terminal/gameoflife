package field;

import cells.ICell;
import cells.ICellFactory;
import configuration.IFieldConfiguration;

public class Field implements IField
{
	private int fieldWidth = 50;
	private int fieldHeight = 50;
	private ICell[][] cells;
	private IFieldConfiguration configuration;

	public Field(int fieldWidth, int fieldHeight, IFieldConfiguration configuration)
	{
		if (fieldWidth < 1 || fieldHeight < 1)
			throw new IllegalArgumentException("Field size negative");

		if(configuration == null)
			throw new IllegalArgumentException("The configuration is null");
		
		this.fieldWidth = fieldWidth;
		this.fieldHeight = fieldHeight;
		this.configuration = configuration;
		this.cells = new ICell[this.fieldWidth][this.fieldHeight];
		this.initCells();
	}

	protected void initCells()
	{
		ICellFactory factory = this.configuration.getInitialCellFactory();
		for (int y = 0; y < this.getHeight(); y++) {
			for (int x = 0; x < this.getWidth(); x++) {
				this.cells[y][x] = factory.createCell(x, y);
			}
		}
	}

	@Override
	public ICell getCell(int x, int y)
	{
		if (x > this.getWidth() - 1 || y > this.getHeight() - 1 || x < 0 || y < 0)
			return this.configuration.getOuterCellFactory().createCell(x, y);

		return this.cells[y][x];
	}

	@Override
	public int getWidth()
	{
		return this.fieldWidth;
	}

	@Override
	public int getHeight()
	{
		return this.fieldHeight;
	}

}
