package field;

import cells.ICell;

public interface IField
{
	/**
	 * Returns the cell with the specified coordinates. What happens if the coordinates are out of
	 * bounds depends on the concrete implementation of the field.
	 * 
	 * @param x the x coordinate of the cell
	 * @param y the y coordinate of the cell
	 * @return the cell with the specified coordinates
	 */
	public ICell getCell(int x, int y);

	/**
	 * Returns the width of the field
	 * 
	 * @return the width of the field
	 */
	public int getWidth();

	/**
	 * Returns the height of the field
	 * 
	 * @return the height of the field
	 */
	public int getHeight();
}
