package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;

import cells.ICell;

public class SwingCell implements ISwingCell
{

	private JButton button;
	private static final Color aliveColor = Color.red;
	private static final Color deadColor = Color.white;

	private ICell innerCell;

	public SwingCell(ICell innerCell)
	{
		this.innerCell = innerCell;

		// initialize the graphical representation
		this.button = new JButton();
		this.button.setSelected(this.innerCell.isAlive());
		this.button.setBackground(this.innerCell.isAlive() ? aliveColor : deadColor);
		// this.button.setBorder(null);
		this.button.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				SwingCell.this.setAlive(!SwingCell.this.isAlive());
				SwingCell.this.commitAlive();
			}
		});

	}

	@Override
	public void setAlive(boolean alive)
	{
		this.innerCell.setAlive(alive);
		this.button.setBackground(alive ? aliveColor : deadColor);
	}

	@Override
	public void commitAlive()
	{
		this.innerCell.commitAlive();
	}

	@Override
	public boolean isAlive()
	{
		return this.innerCell.isAlive();
	}

	@Override
	public int getX()
	{
		return this.innerCell.getX();
	}

	@Override
	public int getY()
	{
		return this.innerCell.getY();
	}

	@Override
	public JComponent getComponent()
	{
		return this.button;
	}
}
