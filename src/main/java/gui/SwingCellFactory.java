package gui;

import cells.ICell;

public class SwingCellFactory implements ISwingCellFactory
{

	@Override
	public ISwingCell createSwingcellFromCell(ICell cell)
	{
		return new SwingCell(cell);
	}

}
