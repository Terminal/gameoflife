package gui;

import javax.swing.JComponent;

import field.IField;

/**
 * A graphical field which can be displayed in a swing panel
 */
public interface ISwingField extends IField
{
	/**
	 * Returns the swing component representing the field
	 * 
	 * @return the swing component representing the field
	 */
	public JComponent getComponent();
}
