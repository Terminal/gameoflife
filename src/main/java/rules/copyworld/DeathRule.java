package rules.copyworld;

import rules.NeighborRule;
import cells.ICell;
import field.IField;

public class DeathRule extends NeighborRule
{

	@Override
	protected boolean processCell(IField field, ICell cell, int livingNeighbors)
	{
		if (livingNeighbors == 0 || livingNeighbors == 2 || livingNeighbors == 4
				|| livingNeighbors == 6 || livingNeighbors == 8) {
			cell.setAlive(false);
			return true;
		}

		return false;
	}

}
